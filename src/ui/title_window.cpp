#include "ui/title_window.h"
#include "app.h"
#include <ncurses.h>
#include <stdexcept>
#include <string>

const std::string TitleWindow::title = app->getAppName() + " " + app->getAppVersion();

TitleWindow::TitleWindow(
    unsigned int height,
    unsigned int width,
    unsigned int start_y,
    unsigned int start_x,
    unsigned int colors)
    : NcWindow(height, width, start_y, start_x, colors)
{
    init();
}

void TitleWindow::init()
{
    werase(win);
    wprintw(win, "%s", TitleWindow::title.c_str());
    wrefresh(win);
}

