#include "ui/command_window.h"
#include "app.h"
#include "ui/cppreadline.h"
#include <iostream>
#include <ncurses.h>
#include <readline/history.h>
#include <readline/readline.h>
#include <string>

#include <cstddef>
#include <cstdlib>

#include <memory>

bool CommandWindow::readline_mode = false;
static inline std::wstring to_wide(const std::string& s);

std::unordered_map<std::string, std::shared_ptr<CppReadline::Console>>
    CommandWindow::readline_consoles;

CommandWindow* CommandWindow::Instance;

int CommandWindow::ui_resize_binding(int, int)
{
    app->ui->resize();
    return 0;
}

bool CommandWindow::readline_initalized = []() {
    //
    // Let ncurses deal with the LINES and COLUMNS environment variables
    //
    rl_change_environment = 0;
    rl_catch_sigwinch = 0;
    rl_deprep_term_function = NULL;
    rl_prep_term_function = NULL;

    rl_bind_key(KEY_RESIZE, ui_resize_binding); // Doesn't work :(

    return true;
}();

CommandWindow::CommandWindow(
    unsigned int height,
    unsigned int width,
    unsigned int start_y,
    unsigned int start_x,
    unsigned int colors)
    : NcWindow(height, width, start_y, start_x, colors)
{
}

std::string CommandWindow::prompt(std::string greeting)
{
    Instance = this;
    rl_redisplay_function = &readlineRedisplayThunk;

    namespace cr = CppReadline;

    std::shared_ptr<cr::Console> c;

    //
    // If an instance of readline object exists with the same greeting,
    // use that
    //
    if (readline_consoles.find(greeting) == readline_consoles.end()) {
        readline_consoles[greeting] = std::make_shared<cr::Console>(greeting);
    }
    c = readline_consoles[greeting];

    readline_mode = true;
    curs_set(2);

    //
    // Handle EOF and Abort Exceptions, assume input is a nullstring
    //
    std::string command;
    try {
        command = c->readLine();
    } catch (CppReadline::Readline_EOF& e) {
        command = "";
    } catch (CppReadline::Readline_Aborted& e) {
        command = "";
    }
    readline_mode = false;

    curs_set(0);
    erase();

    return command;
}

void CommandWindow::readlineRedisplayThunk()
{
    Instance->readlineRedisplay(); // NOT THREAD SAFE
}

//
// This code integrates the Readline instance with ncurses
// reference : https://reversed.top/2016-04-24/ncurses-for-cpp/
//
void CommandWindow::readlineRedisplay()
{
    std::wstring prompt_greeting = to_wide(rl_display_prompt);
    std::wstring linebuf = to_wide(rl_line_buffer);

    std::size_t prompt_width = wcswidth(prompt_greeting.c_str(),
        static_cast<std::size_t>(-1));
    std::size_t cursor_col = prompt_width
        + wcswidth(linebuf.c_str(), rl_point);

    erase();

    std::string prompt_buffer = rl_display_prompt;
    prompt_buffer += rl_line_buffer;

    say(prompt_buffer);

    if (static_cast<unsigned int>(cursor_col) >= width - 1) {
        // Hide the cursor if it lies outside the window. Otherwise it'll
        // appear on the very right.
        curs_set(0);
    } else {
        wmove(win, 0, cursor_col);
    }
}

static inline std::wstring to_wide(const std::string& s)
{
    const std::size_t len = mbstowcs(NULL, s.c_str(), 0);
    if (len == static_cast<std::size_t>(-1)) {
        return std::wstring();
    }

    std::wstring result(len + 1U, L'\0');
    static_cast<void>(mbstowcs(&result[0], s.c_str(), len + 1U));
    return result;
}

void CommandWindow::say(const std::string& msg, const unsigned int color_pair_id)
{
    if (readline_mode == false) {
        werase(win);
        wattron(win, COLOR_PAIR(color_pair_id));
        mvwprintw(win, 0, 0, "%s", msg.c_str());
        wattroff(win, COLOR_PAIR(color_pair_id));
        wrefresh(win);
    }
}
