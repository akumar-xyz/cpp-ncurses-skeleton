#include "cmdline_handler.h"
#include "app_core.h"
#include <string>
#include <vector>
#include <string>
#include <iomanip>


//
// Returns a vector of tokens, handles double quotes and escaped quotes
// Reference : https://stackoverflow.com/a/36567874
//
std::vector<std::string> CmdlineHandler::tokenize( std::string str )
{
    std::vector<std::string> tokens;
    std::stringstream ss(str);

    std::string token;
    while (ss >> std::quoted(token))
    {
        tokens.push_back(token);

    }

    return tokens;
}

CmdlineHandler::CmdlineHandler( std::string command )
{
    //handle empty string
    if (command == "")
    {
        return;
    }

    std::vector<std::string> cmd_tokens = tokenize(command);
    try
    {
        std::function < void(std::vector<std::string>) >
            func = commands.at(cmd_tokens[0]);
        func(cmd_tokens);
    }
    catch (const std::out_of_range &e )
    {
        //
        // If the first word is not a command, check if it is a prefix to some
        // command. Then run it.
        //
        auto it = commands.lower_bound(cmd_tokens[0]);

        // Is it Really a prefix tho?
        if (it->first.compare(0, cmd_tokens[0].size(), cmd_tokens[0]) == 0)
        {
            //
            // If it is also a prefix to the next command, it is unambigious
            //
            if ( std::next(it)->first.compare(0, cmd_tokens[0].size(), cmd_tokens[0]) == 0 )
            {
                //
                // Err: Ambigious prefix
                //
                AppCore::show_error("ERR: Ambiguous Prefix \'" + cmd_tokens[0] + "\'");
            }
            else
            {
                std::function < void(std::vector<std::string>) >
                    func = it->second;
                func(cmd_tokens);
            }

        }
        else
        {
            //
            // Err: Not a command prefix either
            //
            AppCore::show_error("ERR: Unknown Command \'" + cmd_tokens[0] + "\'");
        }
    }
}

//
//
// I realize how goofy the following syntax looks
// TODO: (maybe) clean it up using macros or something
//
std::map
< std::string
, std::function<void( std::vector<std::string> )>
>
CmdlineHandler::commands =
{
    {
        "quit", [](  const std::vector<std::string>& tokens )
        {
            AppCore::app_do(AppCore::QUIT);
        },
    },
};
