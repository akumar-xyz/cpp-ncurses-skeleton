#include "key_bindings.h"
#include "input_handler.h"
#include "app_core.h"

void KeyBindings::register_defaults(InputHandler& handler){
    handler.register_map("q", []( int )
    {
        AppCore::app_do(AppCore::QUIT);
    });
    handler.register_map("ZZ", []( int )
    {
        AppCore::app_do(AppCore::QUIT);
    });
    handler.register_map(":", []( int )
    {
        AppCore::app_do(AppCore::PROMPT_FOR_COMMAND);
    });
    handler.register_map("/", []( int )
    {
        AppCore::app_do(AppCore::PROMPT_FOR_SEARCH_REGEX);
    });
}
    
    
