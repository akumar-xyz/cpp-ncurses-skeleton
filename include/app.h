#ifndef APP_H
#define APP_H

#include "app.h"
#include "ui/ui.h"
#include "input_handler.h"
#include <iostream>

#include <string>

class App
{
private:
    static bool stop_app;
    static const std::string app_name;
    static const std::string app_version;
    void init_ui();

public:

    static UI* ui;
    int run();
    void quit();

    const std::string getAppName();
    const std::string getAppVersion();

    App();
    virtual ~App();
};

extern App* app;

#endif /* APP_H */
