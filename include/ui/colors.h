#ifndef COLORS_H
#define COLORS_H

#include <ncurses.h>

enum ColorIds
{
    A_COLOR_TRANSPARENT       = -1,
    A_COLOR_BLACK       = 0,
    A_COLOR_RED         = 1,
    A_COLOR_GREEN       = 2,
    A_COLOR_YELLOW      = 3,
    A_COLOR_BLUE        = 4,
    A_COLOR_MAGENTA     = 5,
    A_COLOR_CYAN        = 6,
    A_COLOR_WHITE       = 7,
    A_COLOR_B_BLACK     = 8,
    A_COLOR_B_RED       = 9,
    A_COLOR_B_GREEN     = 10,
    A_COLOR_B_YELLOW    = 11,
    A_COLOR_B_BLUE      = 12,
    A_COLOR_B_MAGENTA   = 13,
    A_COLOR_B_CYAN      = 14,
    A_COLOR_B_WHITE     = 15
};

namespace Colors
{

//
// NCurses allows 256 color pairs and 4 bit colors for foreground and 3 bits
// for background. However, background can also be '-1' in case of transparent
// terminals and such. 
//
// For this reason, we will use a 'transparent' background where foreground and
// background would have been the same instead.. cuz that's very useless tbh
// fam
//
void init_color_pairs( void );

unsigned int get_color_id( const ColorIds& fg, const ColorIds& bg );

//
// Overriding the function for cases where fg==bg.
//
unsigned int get_color_id( const ColorIds& );

}

#endif /* COLORS_H */
