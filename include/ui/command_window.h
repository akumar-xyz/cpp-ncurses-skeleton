#ifndef COMMAND_WINDOW_H
#define COMMAND_WINDOW_H

#include "window.h"
#include "cppreadline.h"
#include <string>
#include <unordered_map>
#include <functional>

class CommandWindow : public NcWindow
{
    private:
        void init();
        static void readlineRedisplayThunk();
        void readlineRedisplay();

        static bool readline_initalized;
        static bool readline_mode;
        static CommandWindow* Instance;
        static std::unordered_map
            <std::string, std::shared_ptr<CppReadline::Console>> 
            readline_consoles;

    public:
        CommandWindow(unsigned int, unsigned int,
                unsigned int, unsigned int, unsigned int);
        CommandWindow();

        std::string prompt( std::string );
        using NcWindow::say;
        void say( const std::string& msg, const unsigned int color_pair );

        static int ui_resize_binding( int, int );

        ~CommandWindow();
};

#endif /* COMMAND_WINDOW_H */
