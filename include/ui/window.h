#ifndef WINDOW_H
#define WINDOW_H

#include <ncurses.h>
#include <string>

class NcWindow
{
protected:
	unsigned int height;
	unsigned int width;
	unsigned int start_y;
	unsigned int start_x;
	WINDOW *win;
		

public:
	NcWindow(){}
	NcWindow(unsigned int ,unsigned int ,
			unsigned int ,unsigned int, unsigned int );
	void say(const std::string& );
    void erase( void );
	/* virtual ~Window(); */

};

#endif /* WINDOW_H */
