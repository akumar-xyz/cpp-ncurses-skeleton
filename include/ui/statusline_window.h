#ifndef STATUSLINE_WINDOW_H
#define STATUSLINE_WINDOW_H

#include "window.h"
#include <string>

class StatuslineWindow : public NcWindow
{
private:
    static std::string status;

public:
    /* print_error */
    /* print_status */
    /* print_app_mode */

    void show();
    void set_status(std::string);

    StatuslineWindow(const std::string&, unsigned int, unsigned int,
                     unsigned int, unsigned int, unsigned int);
    /* virtual ~StatuslineWindow(); */
};

#endif /* STATUSLINE_WINDOW_H */
