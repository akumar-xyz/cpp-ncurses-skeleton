#ifndef INPUT_HANDLER_H
#define INPUT_HANDLER_H

#include <string>
#include <unordered_map>
#include <functional>
#include <memory>
#include <chrono>

struct KeyMapNode
{
    std::function<void(int)> func=nullptr;
    std::unordered_map<char, std::shared_ptr<KeyMapNode>> branches;
};

class InputHandler
{
private:
    static int repeat_count;
    static std::shared_ptr<KeyMapNode> keymap_root_node;
    static std::shared_ptr<KeyMapNode> curr_node;
    

public:
    InputHandler();
    virtual ~InputHandler();

    void handle(int);
    void register_map( std::string, std::function<void(int)> );
};

#endif /* INPUT_HANDLER_H */
