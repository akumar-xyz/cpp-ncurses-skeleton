#ifndef KEY_BINDINGS_H
#define KEY_BINDINGS_H

#include "input_handler.h"

namespace KeyBindings
{

void register_defaults( InputHandler& );
    
} /* KeyBindings */ 


#endif /*  */
