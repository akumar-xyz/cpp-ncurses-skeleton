PROJ_NAME := organ
CXX       := c++
CXXFLAGS  := -pedantic-errors -Wall 
LDFLAGS   := -lreadline -lncursesw
BUILD     := ./build
OBJ_DIR   := $(BUILD)/objects
APP_DIR   := $(BUILD)/bin

INCLUDES   := -Iinclude/

SRC       := \
	$(wildcard src/*.cpp)   \
	$(wildcard src/*/*.cpp) \

OBJECTS := $(SRC:%.cpp=$(OBJ_DIR)/%.o)


all: build $(APP_DIR)/$(PROJ_NAME)

$(OBJ_DIR)/%.o: %.cpp
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) $(INCLUDES) -o $@ -c $<

$(APP_DIR)/$(PROJ_NAME): $(OBJECTS)
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) $(INCLUDES) $(LDFLAGS) -o $(APP_DIR)/$(PROJ_NAME) $(OBJECTS)


.PHONY: all build clean debug release
build:
	@mkdir -p $(APP_DIR)
	@mkdir -p $(OBJ_DIR)

debug: CXXFLAGS += -DDEBUG -g
debug: all

release: CXXFLAGS += -O2
release: all

clean:
	-@rm -rvf $(BUILD)
	-@rm -vf ./$(PROJ_NAME)

# run: all
# 	./build/bin/$(PROJ_NAME)
